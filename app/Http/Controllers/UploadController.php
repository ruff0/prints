<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

use App\Document;
use App\User;
use App\Order;
use App\OrderDetail;

use Intervention\Image\ImageManagerStatic;


class UploadController extends Controller
{
    public function add(Request $request) {

 		try{
	 		if( $request->hasFile('files'))
	    	{
        $username = Auth::user()->name;

        $file = $request->file('files');
        $spath = storage_path('app');
        $extension = $file->getClientOriginalExtension();

				Storage::disk('local')->put('/'.$username.'/'.$file->getFilename().'.'.$extension,  File::get($file));
				$entry = new Document();
				$entry->mime = $file->getClientMimeType();
				$entry->original_docname = $file->getClientOriginalName();
				$entry->docname = $file->getFilename().'.'.$extension;
				$entry->save();
        $thumb = ImageManagerStatic::make($file->getRealPath())->resize(200, 200, function ($constraint) {
          $constraint->aspectRatio(); //maintain image ratio
        })->save('/'.$spath.'/'.$username.'/'.'thumb_'.$file->getFilename().'.'.$extension);

	 		}
	 		else{
	 			return response()->json(['error' => 'No Image Found']);
	 		}
	 	}
 		catch(Exception $e){

 		}
		return response()->json(['success' => 'Document uploaded successfully','document_id' => $entry->id,'document_name' => $entry->original_docname]);

	}

	public function get($id){

    $odetail = OrderDetail::where('document_id', '=', $id)->firstOrFail();
    $o = Order::where('order_detail_id', '=', $odetail->id)->firstOrFail();

    $user = User::where('id', '=', $o->user_id)->firstOrFail();

		$entry = Document::where('id', '=', $id)->firstOrFail();
		$file =  Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix($user->name.'/'.$entry->docname);
        return \Response::download($file);
	}
}
