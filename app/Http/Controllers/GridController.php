<?php namespace App\Http\Controllers;

use App\User;
use App\Order;
use Illuminate\Http\Request;
use ViewComponents\Eloquent\EloquentDataProvider;

use ViewComponents\ViewComponents\Input\InputSource;
use ViewComponents\Grids\Grid;
use ViewComponents\Grids\Component\TableCaption;
use ViewComponents\Grids\Component\Column;
use ViewComponents\Grids\Component\DetailsRow;
use ViewComponents\Grids\Component\ColumnSortingControl;
// use ViewComponents\Grids\Component\CsvExport;
use ViewComponents\Grids\Component\PageTotalsRow;
use ViewComponents\ViewComponents\Component\Control\PaginationControl;
use ViewComponents\ViewComponents\Component\Control\PageSizeSelectControl;
use ViewComponents\ViewComponents\Component\Control\FilterControl;
use ViewComponents\ViewComponents\Component\Debug\SymfonyVarDump;
use ViewComponents\ViewComponents\Data\Operation\FilterOperation;
use ViewComponents\ViewComponents\Customization\CssFrameworks\BootstrapStyling;
use ViewComponents\ViewComponents\Customization\CssFrameworks\FoundationStyling;
use ViewComponents\ViewComponents\Customization\CssFrameworks\SemanticUIStyling;
// use ViewComponents\Grids\Component\SolidRow;
use ViewComponents\TestingHelpers\Application\Http\DefaultLayoutTrait;
use ViewComponents\TestingHelpers\Application\Http\TimingTrait;
use ViewComponents\ViewComponents\Component\TemplateView;
use ViewComponents\ViewComponents\Component\ManagedList\ResetButton;
// use ViewComponents\ViewComponents\Component\Layout;

class GridController extends Controller
{

    public function gridorders()
    {


      $provider = new EloquentDataProvider(Order::class);
      $input = new InputSource($_GET);

      $grid = new Grid(
          $provider,
          // all components are optional, you can specify only columns
          [

              new Column('id'),
              new Column('date_time'),
              new Column('status'),

              new Column('amount'),

              // new Column('role'),
              // new Column('birthday'),
              // (new Column('age'))
              //     ->setValueCalculator(function ($row) {
              //         return DateTime
              //             ::createFromFormat('Y-m-d', $row->birthday)
              //             ->diff(new DateTime('now'))
              //             ->y;
              //     })
              //     ->setValueFormatter(function ($val) {
              //         return "$val years";
              //     })
              // ,
              new DetailsRow(new SymfonyVarDump()), // when clicking on data rows, details will be shown
              // new PaginationControl($input->option('page', 1), 10), // 1 - default page, 5 -- page size
              // new PageSizeSelectControl($input->option('page_size', 5), [2, 5, 10]), // allows to select page size
              new ColumnSortingControl('id', $input->option('sort')),
              new ColumnSortingControl('date_time', $input->option('sort')),

              // new ColumnSortingControl('birthday', $input->option('sort')),
              // new CsvExport($input->option('csv')), // yep, that's so simple, you have CSV export now

              (new FilterControl('status', FilterOperation::OPERATOR_EQ, $input('status')))->setView(
                   new TemplateView('select', [
                       'options' => [
                           '' => 'All the status',
                           'unpaid' => 'Unpaid',
                           'paid' => 'Paid',
                           'processing' => 'Processing',
                           'delivering' => 'Delivering',
                           'canceled' => 'Canceled',
                           'completed' => 'Completed',
                           'refunded' => 'Refunded'
                       ]
                   ])
               ),
              // new FilterControl('status', FilterOperation::OPERATOR_LIKE, $input->option('status')),
              new PageTotalsRow([
                // 'id' => PageTotalsRow::OPERATION_IGNORE,
                  'amount' => PageTotalsRow::OPERATION_SUM,
                  // 'age' => PageTotalsRow::OPERATION_AVG




              ]),
                              new ResetButton(),
          ]

      );

          // SemanticUIStyling::applyTo($grid);
          // FoundationStyling::applyTo($grid);
          // BootstrapStyling::applyTo($grid);

      return view('pages.gridorders',['grid' => $grid]);
}
}
