	<!--Side Navbar-->
	<div class="collapse navbar-collapse auto">
	<div id="sidenavbar" class="sidenav">
		<font face='Roboto, sans-serif'>
	  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	  <a href="{{ url('/') }}" onclick="closeNav()">Home</a>
	  @if(\Auth::check())

			<a href="{{ url('/dashboard') }}" onclick="closeNav()">Dashboard</a>
			<a href="{{ url('/gridorders') }}" onclick="closeNav()">Orders</a>

	  	<!-- <a href="{{ url('/print') }}" onclick="closeNav()">Print</a>
	  	<a href="{{ url('/orders') }}" onclick="closeNav()">Orders</a> -->
	  	<a href="{{ url('/adminsettings') }}" onclick="closeNav()">Settings</a>
	  	<a href="{{ url('/logout') }}" onclick="closeNav()">Logout</a>
	  @else
	  	<a href="{{ url('/auth') }}" onclick="closeNav()">Login</a>
	  @endif
		</font>
	</div>
</div>
