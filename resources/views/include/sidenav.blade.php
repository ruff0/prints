	<!--Side Navbar-->
	<div class="collapse navbar-collapse auto">
	<div id="sidenavbar" class="sidenav">
	  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	  <a href="{{ url('/') }}" onclick="closeNav()">Home</a>
	  <a href="{{ url('/#contact') }}" onclick="closeNav()">About</a>
	  <a href="{{ url('/#service') }}" onclick="closeNav()">Services</a>
	  <a href="{{ url('/#contact') }}" onclick="closeNav()">Contact</a>
	  @if(\Auth::check())
	  	<a href="{{ url('/print') }}" onclick="closeNav()">Print</a>
	  	<a href="{{ url('/orders') }}" onclick="closeNav()">Orders</a>
	  	<a href="{{ url('/settings') }}" onclick="closeNav()">Settings</a>
	  	<a href="{{ url('/logout') }}" onclick="closeNav()">Logout</a>
	  @else
	  	<a href="{{ url('/auth') }}" onclick="closeNav()">Login</a>
	  @endif
	</div>
</div>
