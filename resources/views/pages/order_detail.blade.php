@extends('layout.master')

@section('style')
	<!--Custom Styles-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('styles/order_detail.css') }}">
@endsection

@section('content')


	<!--Nav-->
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
      		<a class="navbar-brand" onclick="openNav()"><span class="glyphicon glyphicon-menu-hamburger"></span></a>
    	</div>
    	<div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav navbar-right">
	      @if(\Auth::check())
	        <li class="dropdown">
	        	<a href="{{ url('/') }}" id="username" class="dropdown-toggle" data-toggle="dropdown">Hi, {{ explode(' ',trim(\Auth::user()->name))[0] }}!</a>
	        	<ul class="dropdown-menu">
                    <li><a href="{{ url('/settings') }}" style="font-weight: 100;font-size: 130%">Settings</a></li>
                    <li><a href="{{ url('/print') }}" style="font-weight: 100;font-size: 130%">Print</a></li>
                    <li><a href="{{ url('/orders') }}" style="font-weight: 100;font-size: 130%">Orders</a></li>
                    <li><a href="{{ url('/logout') }}" style="font-weight: 100;font-size: 130%">Logout</a></li>
                </ul>
            </li>
	       	@else
	       	<li>
	        	<a href="{{ url('auth') }}" id="username">Login</a>
	        </li>
	       	@endif
	      </ul>
    	</div>
	  </div>
	</nav>

	<div class="orders">
		<div class="header">
			<div class="title">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							Orders
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="order-detail">
			<a onclick="closeNav()">
			<div class="order-detail-content">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						Order Number :	<b>{{ $orders->id }}</b>
					</div>
					<div class="col-md-6" style="text-align:right;">
						Date : <b>{{$orders->date_time}}</b>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div>

							<?php
							$tp = 'thumb_'.$orders->docname;
							$username = Auth::user()->name;
							$thumbroute = '../../storage/app/'.$username.'/'.$tp;
							// echo $thumbroute;
							?>

							<img src="{{ $thumbroute }}" />


						</div>
						File Uploaded : {{$orders->original_docname}}

					</div>
					<div class="col-md-6">
						<div class="amount">
							$ {{$orders->amount}}
						</div>
						<div style="text-align: right;">
						{{ucfirst(trans("$orders->status"))}}
						<!-- Payment Mode : -->
						@if($orders->payment_mode_id == 1)
							<!-- <b>Card</b> -->
							@if($orders->status == 'unpaid')

								<form action="/payments/{{$orders->id}}" method="POST">
									{{ csrf_field() }}
							  <script
							    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
							    data-key="{{ config('services.stripe.key') }}"
							    data-amount="{{($orders->amount)*100}}"
							    data-name="print system"
							    data-description="{{$orders->original_docname}}"
							    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
							    data-locale="auto"
							    data-zip-code="true">
							  </script>
							</form>

							<font size=2>use card 4242 4242 4242 4242 <br>
								with any ccv and future expiration to test payment.</font>
							@endif

						@else
							<b>Paypal</b>
						@endif
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						Print Details :
						<div class="print-details">
						{{ $orders->color }}
						<br>
						Size : {{ $orders->size }}
						<br>
						{{ $orders->type }}
						<br>
						Cover : {{ $orders->side }}
						<br>
						Quantity : {{ $orders->pages }}
						<br>

						Comment : {{ $orders->comments }}
						</div>
					</div>
					<div class="col-md-6">
						Delivery Address :
						<div class="print-details">
						{{ $orders->name }}
						<br>
						{{ $orders->add1 }}
						<br>
						{{ $orders->add2 }}
						<br>
						{{ $orders->city }} - {{ $orders->pincode }}
						<br>
						Phone : {{ $orders->phone }}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">

					</div>
					<div class="col-md-6" style="text-align:right;">

					</div>
				</div>
			</div>
			</div>
		</a>
		</div>
	</div>



@endsection

@section('script')

@endsection
