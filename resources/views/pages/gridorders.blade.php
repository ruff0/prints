<?php
use ViewComponents\ViewComponents\Customization\CssFrameworks\SemanticUIStyling;
// use ViewComponents\ViewComponents\Customization\CssFrameworks\BootstrapStyling;

?>


@extends('layout.adminmaster')

@section('style')
	<!--Custom Styles-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('styles/order.css') }}">
@endsection

@section('content')


	<!--Nav--><font face='Roboto, sans-serif'>
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
      		<a class="navbar-brand" onclick="openNav()"><span class="glyphicon glyphicon-menu-hamburger"></span></a>
    	</div>
    	<div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav navbar-right">
	      @if(\Auth::check())
	        <li class="dropdown">
	        	<a href="{{ url('/') }}" id="username" class="dropdown-toggle" data-toggle="dropdown">Hi, {{ explode(' ',trim(\Auth::user()->name))[0] }}!</a>
	        	<ul class="dropdown-menu">
                    <li><a href="{{ url('/settings') }}" style="font-weight: 100;font-size: 130%">Settings</a></li>
                    <li><a href="{{ url('/logout') }}" style="font-weight: 100;font-size: 130%">Logout</a></li>
                </ul>
            </li>
	       	@else
	       	<li>
	        	<a href="{{ url('auth') }}" id="username">Login</a>
	        </li>
	       	@endif
	      </ul>
    </div>
	  </div>
	</nav>
	<div class="orders">
		<div class="header">
			<div class="title">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							Orders
						</div>
					</div>
				</div>
			</div>
		</div>
	</font>
	@endsection
@section('grid')
<div>
	<?php  SemanticUIStyling::applyTo($grid);
	echo $grid?>
</div>
</div>
@endsection


@section('script')

@endsection
