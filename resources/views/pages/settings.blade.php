@extends('layout.master')

@section('style')
	<!--Custom Styles-->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('styles/print.css') }}">
@endsection

@section('content')

  <meta name="_token" content="{{ csrf_token() }}"/>
  <meta http-equiv="cache-control" content="max-age=0" />
  <meta http-equiv="cache-control" content="no-cache" />
  <meta http-equiv="expires" content="0" />
  <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
  <meta http-equiv="pragma" content="no-cache" />



	<!--Nav-->
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
      		<a class="navbar-brand" onclick="openNav()"><span class="glyphicon glyphicon-menu-hamburger"></span></a>
    	</div>
    	<div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav navbar-right">
	      @if(\Auth::check())
	        <li class="dropdown">
	        	<a href="{{ url('/') }}" id="username" class="dropdown-toggle" data-toggle="dropdown">Hi, {{ explode(' ',trim(\Auth::user()->name))[0] }}!</a>
	        	<ul class="dropdown-menu">
                    <li><a href="{{ url('/settings') }}" style="font-weight: 100;font-size: 130%">Settings</a></li>
                    <li><a href="{{ url('/print') }}" style="font-weight: 100;font-size: 130%">Print</a></li>
                    <li><a href="{{ url('/orders') }}" style="font-weight: 100;font-size: 130%">Orders</a></li>
                    <li><a href="{{ url('/logout') }}" style="font-weight: 100;font-size: 130%">Logout</a></li>
                </ul>
            </li>
	       	@else
	       	<li>
	        	<a href="{{ url('auth') }}" id="username">Login</a>
	        </li>
	       	@endif
	      </ul>
    </div>
	  </div>
	</nav>

	<div class="settings">
		<div class="header">
			<div class="title">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
						Settings
						</div>
					</div>
				</div>
			</div>
		</div>
		<a onclick="closeNav()">
		<div class="setting-content">
			Name : {{ \Auth::user()->name }}
			<BR><br>
			Email : {{ \Auth::user()->email }}

		</div>
		<hr>
		<div id="delivery-address">
		<div style="font-weight: 300;font-size:150%;padding: 0px 40px;">Address :</div>
        <ul class="row" style="list-style: none;" id="ul-addr">
          @if($user_address)
          @foreach($user_address as $adds)
          <li class="col-md-3">
            <div class="address-content">
              <div class="address">
                <div class="radio">
                  <label>
										<!-- <input type="radio" name="address" value="{{ $adds->id }}"> -->
										{{ $adds->id }}
										&nbsp;&nbsp;
										<b>{{ $adds->name }}</b>
									</label>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<a id="delete_addr" style="color:red;cursor:pointer;" at="{{ $adds->id }}">
										<span class="glyphicon glyphicon-trash"></span></a>
                </div>
                <p>{{ $adds->add1 }}</p>
                <p>{{ $adds->add2 }}</p>
                <p>{{ $adds->city." - ".$adds->pincode }}</p>
                <p>Mobile : {{ $adds->phone }}</p>
              </div>
            </div>
          </li>
          @endforeach
          @endif

          <li class="col-md-3">
            <a href="" id="add-address" data-toggle="modal" data-target="#addadd">
            <div class="address-content">
              <div class="address">
                <span class="glyphicon glyphicon-plus"></span>
                <p>Add a new address</p>
              </div>
            </div>
            </a>
          </li>
        </ul>
        </div>
			</a>
	</div>

	<!--Add Address Modal-->
	<div id="addadd" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<a onclick="closeNav()">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add New Address</h4>
			</div>
			<div class="modal-body">
				<div class="new-address">

					<div class="form-group">
					<input type="text" name="address-name" id="address-name" placeholder="Name (Home, Work, etc.)" class="form-control">
					</div>
					<div class="form-group">
					<input type="text" name="address-line1" id="address-line1" placeholder="Address Line 1" class="form-control">
					</div>
					<div class="form-group">
					<input type="text" name="address-line2" id="address-line2" placeholder="Address Line 2" class="form-control">
					</div>
					<div class="form-group">
					<input type="text" name="address-city" id="address-city" placeholder="City" class="form-control">
					</div>
					<div class="form-group">
					<input type="text" name="address-pincode" id="address-pincode" placeholder="Pincode" class="form-control">
					</div>
					<div class="form-group">
					<input type="text" name="address-mobile" id="address-mobile" placeholder="Mobile" class="form-control">
					</div>
				</div>
			</div>
			<div class="modal-footer">
					<div class="error" id="error" align="left">
					</div>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" id="add-addr" class="btn btn-primary">Add</button>
			</div>
		</div>
	</a>

	</div>
	</div>
@endsection

@section('script')
<script type="text/javascript">

$("#add-addr").click(function(){
	var name = $("#address-name").val();
	var add1 = $("#address-line1").val();
	var add2 = $("#address-line2").val();
	var city = $("#address-city").val();
	var pin = $("#address-pincode").val();
	var mob = $("#address-mobile").val();

	if(!name.trim() || !add1.trim() || !add2.trim() || !city.trim() || !pin.trim() || !mob.trim()){
		$("#error").text("Some field(s) missing!");
	}
	else{
		$("#addadd").modal('toggle');
		var data = {"name":name,"add1":add1,"add2":add2,"city":city,"pin":pin,"mob":mob}
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			url: "{{ url('/address/add')}}",
			method: 'POST',
			data: data,
			dataType: 'json',
			success: function(response) {
				$("#delivery-address").html('');
				$("#delivery-address").append('<ul class="row" style="list-style: none;" id="ul-addr">');

				if(response.addr){
					$.each(response.addr,function(index, value){
						$("#ul-addr").append('<li class="col-md-3"><div class="address-content"><div class="address"><div class="radio"><label>'+value.id+'&nbsp;&nbsp;<b>'+value.name+'</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="delete_addr" style="color:red;cursor:pointer;" at="'+value.id+'"><span class="glyphicon glyphicon-trash"></span></a></div><p>'+value.add1+'</p><p>'+value.add2+'</p><p>'+value.city+' - '+value.pincode+'</p><p>Mobile : '+value.phone+'</p></div></div></li>');
					});
				}
				else{
					$("#delivery-address").html('Server Failed!')
				}
				$("#ul-addr").append('<li class="col-md-3"><a href="" id="add-address" data-toggle="modal" data-target="#addadd"><div class="address-content"><div class="address"><span class="glyphicon glyphicon-plus"></span><p>Add a new address</p></div></div></a></li></ul>');
			},
			// beforeSend: function() {
			// 		gif_url = "{{ URL::asset('/images/91.gif') }}";
			// 		$("#delivery-address").html("");
			// 		$("#delivery-address").html('<div class="row"><div class="col-md-12"><div align="center"><img src='+gif_url+'></img></div></div></div><br>');
      //
			//  },
			error: function (xhr, ajaxOptions, thrownError) {
						 console.log(xhr.status);
						 console.log(xhr.responseText);
						 console.log(thrownError);
			}

		});

	}
});


$("#delivery-address").on('click','a#delete_addr',function(){
	var addr_id = $(this).attr('at');
	var data = {'address_id':addr_id}
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			url: "{{ url('/address/delete')}}",
			method: 'POST',
			data: data,
			dataType: 'json',
			success: function(response) {
					$("#delivery-address").html('');
					$("#delivery-address").append('<ul class="row" style="list-style: none;" id="ul-addr">');
					if(response.addr){
						$.each(response.addr,function(index, value){
							$("#ul-addr").append('<li class="col-md-3"><div class="address-content"><div class="address"><div class="radio"><label>'+value.id+'&nbsp;&nbsp;<b>'+value.name+'</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="delete_addr" style="color:red;cursor:pointer;" at="'+value.id+'"><span class="glyphicon glyphicon-trash"></span></a></div><p>'+value.add1+'</p><p>'+value.add2+'</p><p>'+value.city+' - '+value.pincode+'</p><p>Mobile : '+value.phone+'</p></div></div></li>');
						});
				}
				else{
					$("#delivery-address").html('Server Failed!')
				}
				$("#ul-addr").append('<li class="col-md-3"><a href="" id="add-address" data-toggle="modal" data-target="#addadd"><div class="address-content"><div class="address"><span class="glyphicon glyphicon-plus"></span><p>Add a new address</p></div></div></a></li></ul>');
			},
			error: function (xhr, ajaxOptions, thrownError) {
						 console.log(xhr.status);
						 console.log(xhr.responseText);
						 console.log(thrownError);
			}
		});
});



</script>
@endsection
