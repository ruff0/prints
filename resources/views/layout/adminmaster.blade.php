<!DOCTYPE html>
<html lang="en">
	<head>
	@include('include.head')
	@yield('style')
	</head>
	<body>
		@include('include.adminsidenav')
		@yield('content')
		@yield('grid')
		@include('include.scripts')
		@yield('script')
	</body>
</html>
